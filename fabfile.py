import os
from fabric.api import run, env, cd, roles, task, warn, hosts, sudo, put
from fabric.contrib.files import sed, contains
from fabric.colors import green, red

host = os.environ['abbatfaria_host']
user = os.environ['abbatfaria_user']
domain = os.environ['smevna_domain']


def production_env():
    """Окружение для Abbat Faria"""

    env.key_filename = [os.path.join(os.environ['HOME'], '.ssh', 'id_rsa')]
    env.hosts = [host]
    env.user = user
#    env.passw = passw
    env.project_root = '/home'  # Путь до каталога проекта (на сервере)
    # env.shell = '/usr/local/bin/bash -c'  # Используем шелл отличный от умолчательного (на сервере)

@task
@hosts(host)
def install_vps():
    ''' Установить все пакеты и настройки VPS'''

    production_env()
    ssh_protect()
    setup_python()
    setup_nginx()
    create_user() # только после setup_nginx()
    create_app()


@task()
@hosts(host)
def ssh_protect():
    '''Запретить SSH-доступ по паролю. Разрешается только по ключу.'''

    with cd('/etc/ssh'):
        sed(filename='sshd_config', 
            before='PasswordAuthentication yes',
            after='PasswordAuthentication no',
            backup='.bak', flags='i'
        )
        sed(filename='sshd_config', 
            before='UsePAM yes',
            after='UsePAM no',
            backup='.bak', flags='i'
        )
        config_not_changedPA = contains(filename='sshd_config',
            text='PasswordAuthentication yes',
            exact=False, use_sudo=False,
            escape=True, 
            shell=False, 
            case_sensitive=True
        )
        config_not_changedPAM = contains(filename='sshd_config',
            text='UsePAM yes',
            exact=False, use_sudo=False,
            escape=True, 
            shell=False, 
            case_sensitive=True
        )
        if config_not_changedPA or config_not_changedPAM:
            warn(red("Файл 'sshd_config' не был изменён! SSH-доступ по паролю всё ещё разрешён!"))
    run('service sshd restart')


@task
@hosts(host)
def setup_python():
    '''Установить Python, Pip, Virtualenv и прочие модули питона'''

    run('apt-get install python3 python3-dev build-essential virtualenv')

@task
@hosts(host)
def setup_nginx():
    '''Установить Nginx и настройки'''

    run('apt-get install nginx')
    put('smevna_nginx.conf', '/etc/nginx/sites-available/{domain}.conf'.format(domain))
    run('ln -s /etc/nginx/sites-available/{domain}.conf /etc/nginx/sites-enabled'.format(domain))
    run('sudo nginx -t')
    run('systemctl restart nginx')

@task
@hosts(host)
def setup_uwsgi():
    '''Установить и активировать uWSGI'''

    run('sudo su - smevna -s /bin/bash')
    run('virtualenv -p python3 venv')
    run('source venv/bin/activate')
    run('pip install uwsgi')

@task
@hosts(host)
def debug_enable():
    '''
    Включает режим отладки на Production VPS.
    ВНИМАНИЕ: необходимо отключить режим отладки по окончании работ, поскольку это небезопасно.
    '''

    # http://www.ultrabug.fr/nginx-conditional-uwsgi-error-handling/
    # https://uwsgi-docs.readthedocs.io/en/latest/Options.html#catch-exceptions
    # https://kecko.wordpress.com/2014/01/24/setting-up-uwsgi-nginx-flask-and-mysql-on-ubuntu/
    put('smevna_wsgi_debugmode.ini', '/home/smevna/{}/smevna_wsgi.ini'.format(domain), mode='0o755')
    put('smevna_nginx_debugmode.conf', '/etc/nginx/sites-available/{domain}.conf'.format(domain))
    
    run('systemctl stop nginx')
    run('systemctl stop smevna')
    
    run('systemctl start nginx')
    run('systemctl start smevna')



@task
@hosts(host)
def create_user():
    ''' Создать пользователя'''

    run('useradd -m smevna')
    run('mkdir /home/smevna/{}'.format(domain))

@task
@hosts(host)
def create_app():
    ''' Создать тестовое Flask-приложение, настроить вирт.среду.
    Запустить uWSGI в качестве сервиса'''

    put('app.py', '/home/smevna/{}/app.py', mode='0o755')
    run('chown -Rc smevna:www-data /home/smevna')
    run('chmod -R 0755 /home/smevna')
    run('sudo su - smevna -s /bin/bash')
    run('virtualenv -p python3 venv')
    run('source venv/bin/activate')
    run('pip install flask')
    
    # uWSGI default setup as a service
    put('smevna_wsgi.ini', '/home/smevna/{}/smevna_wsgi.ini'.format(domain), mode='0o755')
    put('smevna.service', '/etc/systemd/system/smevna.service')
    run('systemctl start smevna')
    run('systemctl enable smevna')
    run('systemctl status smevna')



